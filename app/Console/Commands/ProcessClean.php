<?php

namespace App\Console\Commands;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Config;
use App\Models\Data;
use Illuminate\Console\Command;
use Exception;
use Illuminate\Support\Facades\Http;

class ProcessClean extends Command
{


    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'procesar:nombres';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Proceso llena los nombre y apellidos que faltan en los campos.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     * php artisan schedule:run
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Iniciando proceso nombres.');


        $totalrows = DB::table('data')->count();
        $this->info("total registro : " . $totalrows );
        $this->info("");
        $this->info("");
        $this->info("==========================================================");
        $this->info("");
        $this->info("");


        Data::select('id', 'nombres','apellidos', 'num_doc', 'status_name', 'status')->chunk(200, function ($rows) {

            foreach ($rows as $row) { 
                $dni = $row["num_doc"];

                $this->info(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
                $this->info("peticion : " . $dni . " pos:" . $row["id"] );

                if (strlen($dni) < 8 || strlen($dni) > 5) {
                  //  $dni = $row["new_num_doc"];
                } 


                $url = 'https://dni.optimizeperu.com/api/persons/' . $dni;
                $response = Http::get($url);

                if ($response->successful() && isset($response['name']) ) {
                    $affected = DB::table('data')->where('id', $row['id'])->update(
                        [
                            'nombres' => $response['name'],
                            'apellidos' => $response['first_name']. ' ' .$response['last_name'] ,
                        ]
                    );

                    $this->info("resp: " . $response['name'] . ">>>");
                } else {
                    $this->error("resp: no encuentra api >>>");

                    $affected = DB::table('data')->where('id', $row['id'])->update(
                        [
                            'status' => 0,
                            'status_name' => 'dni:no valido api'
                        ]
                    );
                }

                $this->info("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
                $this->info("");
                $this->info(""); 
            }
        });
            

        $this->info("total registro : " . $totalrows );


    }

  
}
