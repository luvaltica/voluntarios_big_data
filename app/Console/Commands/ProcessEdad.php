<?php

namespace App\Console\Commands;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Config;
use App\Models\Data;
use Illuminate\Console\Command;
use Exception;
use Illuminate\Support\Facades\Http;

class ProcessEdad extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'procesar:edad';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Proceso calcula la edad';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     * php artisan schedule:run
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Iniciando proceso Edades.');
        $totalrows = DB::table('data')->count();

        $this->info("total registro : " . $totalrows );

        $this->info("");
        $this->info("");
        $this->info("==========================================================");
        $this->info("");
        $this->info("");


        Data::select('id', 'num_doc', 'f_nacimiento')->chunk(200, function ($rows) {
            foreach ($rows as $row) { 
                $f_nac = trim($row["f_nacimiento"]);
                $afnac = explode("/", $f_nac);
                
                $this->info(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
                $this->info("peticion : " . $f_nac . " pos:" . $row["id"] );

                if (!empty($f_nac)) {

                    if (count($afnac)==3) {
                        $dia = $afnac[0];
                        $mes = $afnac[1];
                        $ano = $afnac[2];

                        $new_fecha_nac = $ano . "-" . $mes . "-" . $dia;

                        if (($timestamp = strtotime($new_fecha_nac)) === false) {
                            $this->error("fecha nac no se pude parsear : " . $f_nac . ">>>>>");
                            $affected = DB::table('data')->where('id', $row["id"])->update(
                                [
                                    'status' => 3,
                                    'status_name' => $row["status_name"] . '|f_nac: no se puede parsear'
                                ]
                            );
                            
                        } else {
                            $nueva = date('Y-m-d', strtotime($new_fecha_nac));
                            $tiempo = strtotime($nueva); 
                            $ahora = time(); 
                            $edad = ($ahora-$tiempo)/(60*60*24*365.25); 
                            $edad = floor($edad); 

                            if ($edad > 0) {
                                $affected = DB::table('data')->where('id', $row["id"])->update(
                                    [
                                        'edad' => $edad
                                    ]
                                );
                                $this->info("edad : " . $edad . ">>>>" );
                            } else {
                                $this->error("fecha nac es menor que cero : " . $f_nac . ">>>>>");
                                $affected = DB::table('data')->where('id', $row["id"])->update(
                                    [
                                        'status' => 0,
                                        'status_name' => $row["status_name"] . '|f_nac: es menor que cero'
                                    ]
                                );
                            }

                        }

                    } else {
                        $this->error("fecha nac no esta completo : " . $f_nac . ">>>>>");
                        $affected = DB::table('data')->where('id', $row["id"])->update(
                            [
                                'status' => 0,
                                'status_name' => $row["status_name"] . '|f_nac: no esta completo'
                            ]
                        );
                    }


                } else {
                    $this->error("fecha nac es null : " . $f_nac . ">>>>>");
                    $affected = DB::table('data')->where('id', $row["id"])->update(
                        [
                            'status' => 3,
                            'status_name' => $row["status_name"] . '|f_nac: es vacio'
                        ]
                    );
                }
                
                $this->info("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
                $this->info("");
                $this->info("");

            }
        });

            
        $this->info('termino proceso edades .');
    }


  
}
