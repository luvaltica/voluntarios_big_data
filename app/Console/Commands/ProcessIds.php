<?php

namespace App\Console\Commands;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Config;
use App\Models\Data;
use Illuminate\Console\Command;
use Exception;
use Illuminate\Support\Facades\Http;

class ProcessIds extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'procesar:ids';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Proceso llena ids incrementalmentre';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     * php artisan schedule:run
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Iniciando proceso ids.');


        $data = Data::select('id')->get();

        $this->info("total registro : " . count($data) );


        $count = 1;
        foreach ($data as $row) {
           $affected = DB::table('data')->update(
                    [
                        'id' => $count
                    ]
            );
           $count++;

            $this->info("update id : " . $count );

        }
            
        $this->info('termino proceso .');


    }


  
}
