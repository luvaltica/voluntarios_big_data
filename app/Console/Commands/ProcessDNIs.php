<?php

namespace App\Console\Commands;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Config;
use App\Models\Data;
use Illuminate\Console\Command;
use Exception;
use Illuminate\Support\Facades\Http;

class ProcessDNIs extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'procesar:dnis';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Proceso verifica el formato DNIS';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     * php artisan schedule:run
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Iniciando proceso Emails.');

        $totalrows = DB::table('data')->count();
        $this->info("total registro : " . $totalrows );
        $this->info("");
        $this->info("");
        $this->info("==========================================================");
        $this->info("");
        $this->info("");


        Data::select('id', 'num_doc')->chunk(200, function ($rows) {
            foreach ($rows as $row) { 
                $num_doc = trim($row["num_doc"]);
                $this->info(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
                $this->info("peticion : " . $num_doc . " pos:" . $row["id"] );

                if (!empty($num_doc)) {

                    if (strlen($num_doc) > 8) {
                        $this->info("num doc no es dni : " . $num_doc );
                        $affected = DB::table('data')->where('id', $row["id"])->update(
                            [
                                'status' => 1,
                                'status_name' => $row["status_name"] . '|num_doc: no es dni validar manualmente'
                            ]
                        );

                    }

                    if (strlen($num_doc) < 6) {
                        $this->error("num doc es menor que 6 : " . $num_doc );
                        $affected = DB::table('data')->where('id', $row["id"])->update(
                            [
                                'status' => 0,
                                'status_name' => $row["status_name"] . '|num_doc: es menor que 6'
                            ]
                        );
                    }

                    if (strlen($num_doc) == 6) {
                        $this->error("num doc se completara con dos ceros por delante : " . $num_doc );

                        $affected = DB::table('data')->where('id', $row["id"])->update(
                            [
                                'status' => 3,
                                'new_num_doc' => '00'. $row["num_doc"],
                                'status_name' => $row["status_name"] . '|num_doc: se completa con dos ceros por delante'
                            ]
                        );
                    }

                    if (strlen($num_doc) == 7) {
                        $this->error("num doc se completara con cero por delante : " . $num_doc );

                        $affected = DB::table('data')->where('id', $row["id"])->update(
                            [
                                'status' => 3,
                                'new_num_doc' => '0'. $row["num_doc"],
                                'status_name' => $row["status_name"] . '|num_doc: se completa con cero por delante'
                            ]
                        );
                    }

                } else {
                    $this->error("num doc es null : " . $num_doc );
                    $affected = DB::table('data')->where('id', $row["id"])->update(
                        [
                            'status' => 0,
                            'status_name' => $row["status_name"] . '|num_doc: es vacio'
                        ]
                    );
                }     

                
                $this->info("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
                $this->info("");
                $this->info("");

            }
        });
            
        $this->info('termino proceso emails .');
    }


  
}
