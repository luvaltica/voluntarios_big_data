<?php

namespace App\Console\Commands;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Config;
use App\Models\Data;
use Illuminate\Console\Command;
use Exception;
use Illuminate\Support\Facades\Http;

class ProcessEmails extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'procesar:emails';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Proceso verifica el formato emails';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     * php artisan schedule:run
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Iniciando proceso Emails.');

        $totalrows = DB::table('data')->count();
        $this->info("total registro : " . $totalrows );
        $this->info("");
        $this->info("");
        $this->info("==========================================================");
        $this->info("");
        $this->info("");


        Data::select('id', 'email', 'num_doc')->chunk(200, function ($rows) {
            foreach ($rows as $row) { 
                $email = $row["email"];

                $this->info(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
                $this->info("peticion : " . $email . " pos:" . $row["id"] );

                if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
                    $this->info("email valido : " . $email );
                } else {
                    $this->error("email no valido : " . $email );

                    $affected = DB::table('data')->where('num_doc', $row["num_doc"])->update(
                        [
                            'status' => 0,
                            'status_name' => $row["status_name"] . '|email:no formato (valido)'
                        ]
                    );
                }

                $this->info("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
                $this->info("");
                $this->info(""); 
            }
        });
            
        //$this->info('termino proceso emails');
    }
  
}
